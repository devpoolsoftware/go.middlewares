package middlewares

import (
	"bitbucket.org/devpoolsoftware/go.pipeline"
	"github.com/julienschmidt/httprouter"
	"net/http"
)

func Authorize(roles ...string) pipeline.Middleware {
	return func(next httprouter.Handle) httprouter.Handle {
		return func(response http.ResponseWriter, request *http.Request, params httprouter.Params) {

			if request.Context().Value("user") == nil {
				http.Error(response, "Unauthorized", http.StatusUnauthorized)
				return
			}

			var user = request.Context().Value("user").(UserContext)

			if user.Roles == nil || len(user.Roles) == 0 {
				http.Error(response, "Unauthorized", http.StatusUnauthorized)
				return
			}

			for _, role := range roles {
				for _, roleFromUser := range user.Roles {
					if role == roleFromUser {
						next(response, request, params)
						return
					}
				}
			}

			http.Error(response, "Unauthorized", http.StatusUnauthorized)

		}
	}
}
