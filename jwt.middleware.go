package middlewares

import (
	"bitbucket.org/devpoolsoftware/go.pipeline"
	"context"
	"encoding/json"
	"github.com/dgrijalva/jwt-go"
	"github.com/google/uuid"
	"github.com/julienschmidt/httprouter"
	"log"
	"net/http"
	"strings"
)

type UserContext struct {
	Id    uuid.UUID
	Roles []string
}

func getTokenFromHeader(request *http.Request) (tokenString string) {

	var authorizationHeader = request.Header.Get("Authorization")

	if authorizationHeader != "" {

		var splitAuthorizationHeader = strings.Split(authorizationHeader, "Bearer")

		if len(splitAuthorizationHeader) > 0 {
			tokenString = strings.Trim(splitAuthorizationHeader[1], " ")
		}

	}

	return tokenString
}

func Jwt(key string) pipeline.Middleware {
	return func(next httprouter.Handle) httprouter.Handle {
		return func(response http.ResponseWriter, request *http.Request, params httprouter.Params) {

			var currentContext = request.Context()

			if currentContext == nil {
				currentContext = context.Background()
			}

			var tokenString = getTokenFromHeader(request)

			if tokenString == "" {
				next(response, request.WithContext(currentContext), params)
				return
			}

			var getKeyFunc = func(token *jwt.Token) (interface{}, error) {
				return []byte(key), nil
			}

			token, err := jwt.Parse(tokenString, getKeyFunc)
			if err != nil {
				log.Println(err)
				response.WriteHeader(http.StatusBadRequest)
				return
			}

			claims, ok := token.Claims.(jwt.MapClaims)
			if ok == false && token.Valid == false {
				response.WriteHeader(http.StatusBadRequest)
				return
			}

			var user UserContext

			if claims["id"] != nil {
				var id, err = uuid.Parse(claims["id"].(string))
				if err != nil {
					log.Println(err)
				}
				user.Id = id
			}

			if claims["roles"] != nil {
				var roles []string
				json.Unmarshal([]byte(claims["roles"].(string)), &roles)
				user.Roles = roles
			}

			currentContext = context.WithValue(currentContext, "user", user)
			next(response, request.WithContext(currentContext), params)


		}
	}
}
