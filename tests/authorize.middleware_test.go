package tests

import (
	"bitbucket.org/devpoolsoftware/go.middlewares"
	"context"
	"github.com/google/uuid"
	"github.com/julienschmidt/httprouter"
	"net/http"
	"net/http/httptest"
	"testing"
)

func Test_get_access_handler_not_authorize(t *testing.T) {

	request, err := http.NewRequest("GET", "/api/users", nil)

	if err != nil {
		t.Fatal(err)
	}

	responseRecorder := httptest.NewRecorder()

	middleware := middlewares.Authorize("Adm")

	handle := middleware(nil)

	handle(responseRecorder, request, nil)

	if status := responseRecorder.Code; status != http.StatusUnauthorized {
		t.Errorf("handler returned wrong status code: got %v want %v", status, http.StatusUnauthorized)
	}

}

func Test_get_access_handler_authorize(t *testing.T) {

	request, err := http.NewRequest("GET", "/api/users", nil)

	if err != nil {
		t.Fatal(err)
	}

	responseRecorder := httptest.NewRecorder()

	middleware := middlewares.Authorize("Adm")

	handle := middleware(func(writer http.ResponseWriter, request *http.Request, params httprouter.Params) {

	})

	user := middlewares.UserContext{Id: uuid.New(), Roles: []string{"Adm", "User"}}

	ctx := context.WithValue(context.Background(), "user", user)

	request = request.WithContext(ctx)
	handle(responseRecorder, request, nil)

	if status := responseRecorder.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v", status, http.StatusOK)
	}

}

func Test_get_access_handler_authorize_not_role(t *testing.T) {

	request, err := http.NewRequest("GET", "/api/users", nil)

	if err != nil {
		t.Fatal(err)
	}

	responseRecorder := httptest.NewRecorder()

	middleware := middlewares.Authorize("Adm")

	handler := middleware(func(writer http.ResponseWriter, request *http.Request, params httprouter.Params) {

	})

	user := middlewares.UserContext{Id: uuid.New(), Roles: []string{"Adms", "User"}}

	ctx := context.WithValue(context.Background(), "user", user)

	request = request.WithContext(ctx)
	handler(responseRecorder, request, nil)

	if status := responseRecorder.Code; status != http.StatusUnauthorized {
		t.Errorf("handler returned wrong status code: got %v want %v", status, http.StatusUnauthorized)
	}

}
