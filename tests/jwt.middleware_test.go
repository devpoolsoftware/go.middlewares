package tests

import (
	"bitbucket.org/devpoolsoftware/go.middlewares"
	"encoding/json"
	"github.com/dgrijalva/jwt-go"
	"github.com/google/uuid"
	"github.com/julienschmidt/httprouter"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"
)

var secretKey = "topSecretKey123"

func Test_request_without_token(t *testing.T) {

	// e.g. GET /api/projects?page=1&per_page=100
	request, err := http.NewRequest("GET", "/api/projects", nil)

	if err != nil {
		t.Fatal(err)
	}

	// Our handler might also expect an API key.
	//req.Header.Set("Authorization", "Bearer abc123")

	responseRecorder := httptest.NewRecorder()

	middleware := middlewares.Jwt(secretKey)

	handler := middleware(func(writer http.ResponseWriter, request *http.Request, params httprouter.Params) {
		writer.WriteHeader(http.StatusOK)
	})

	handler(responseRecorder, request, nil)

	if status := responseRecorder.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v", status, http.StatusOK)
	}

}

func Test_request_with_bad_format_token(t *testing.T) {

	// e.g. GET /api/projects?page=1&per_page=100
	request, err := http.NewRequest("GET", "/api/projects", nil)

	if err != nil {
		t.Fatal(err)
	}

	request.Header.Set("Authorization", "Bearer bad.format.token")

	responseRecorder := httptest.NewRecorder()

	middleware := middlewares.Jwt(secretKey)

	handler := middleware(func(writer http.ResponseWriter, request *http.Request, params httprouter.Params) {
		writer.WriteHeader(http.StatusOK)
	})

	handler(responseRecorder, request, nil)

	if status := responseRecorder.Code; status != http.StatusBadRequest {
		t.Errorf("handler returned wrong status code: got %v want %v", status, http.StatusBadRequest)
	}

}

func Test_request_good_token(t *testing.T) {

	request, err := http.NewRequest("GET", "/api/projects", nil)

	if err != nil {
		t.Fatal(err)
	}

	token, err := generateToken(5, []string{"Adm"}, true)

	request.Header.Set("Authorization", "Bearer "+token)

	responseRecorder := httptest.NewRecorder()

	middleware := middlewares.Jwt(secretKey)

	handler := middleware(func(writer http.ResponseWriter, request *http.Request, params httprouter.Params) {

		if request.Context().Value("user") == nil {
			http.Error(writer, "Unauthorized", http.StatusUnauthorized)
			return
		}

		var user = request.Context().Value("user").(middlewares.UserContext)

		if user.Roles[0] == "Adm" {
			http.Error(writer, "Unauthorized", http.StatusOK)
			return
		}

		writer.WriteHeader(http.StatusUnauthorized)
	})

	handler(responseRecorder, request, nil)

	if status := responseRecorder.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v", status, http.StatusOK)
	}

}

func Test_request_bad_exp(t *testing.T) {

	request, err := http.NewRequest("GET", "/api/projects", nil)

	if err != nil {
		t.Fatal(err)
	}

	token, err := generateToken(-5, []string{"Adm"}, true)

	request.Header.Set("Authorization", "Bearer "+token)

	responseRecorder := httptest.NewRecorder()

	middleware := middlewares.Jwt(secretKey)

	handler := middleware(func(writer http.ResponseWriter, request *http.Request, params httprouter.Params) {

		if request.Context().Value("user") == nil {
			http.Error(writer, "Unauthorized", http.StatusUnauthorized)
			return
		}

		var user = request.Context().Value("user").(middlewares.UserContext)

		if user.Roles[0] == "Adm" {
			http.Error(writer, "Unauthorized", http.StatusOK)
			return
		}

		writer.WriteHeader(http.StatusBadRequest)
	})

	handler(responseRecorder, request, nil)

	if status := responseRecorder.Code; status != http.StatusBadRequest {
		t.Errorf("handler returned wrong status code: got %v want %v", status, http.StatusBadRequest)
	}

}
func Test_request_bad_struct(t *testing.T) {

	request, err := http.NewRequest("GET", "/api/projects", nil)

	if err != nil {
		t.Fatal(err)
	}

	token, err := generateToken(5, []string{}, false)

	request.Header.Set("Authorization", "Bearer "+token)

	responseRecorder := httptest.NewRecorder()

	middleware := middlewares.Jwt(secretKey)

	handler := middleware(func(writer http.ResponseWriter, request *http.Request, params httprouter.Params) {

		if request.Context().Value("user") == nil {
			http.Error(writer, "Unauthorized", http.StatusUnauthorized)
			return
		}

		var user = request.Context().Value("user").(middlewares.UserContext)

		if len(user.Roles) > 0 && user.Roles[0] == "Adm" {
			http.Error(writer, "Unauthorized", http.StatusOK)
			return
		}

		writer.WriteHeader(http.StatusBadRequest)
	})

	handler(responseRecorder, request, nil)

	if status := responseRecorder.Code; status != http.StatusBadRequest {
		t.Errorf("handler returned wrong status code: got %v want %v", status, http.StatusBadRequest)
	}

}

func generateToken(exper int64, roles []string, addUser bool) (string, error) {

	var exp = time.Now().Add(time.Minute * time.Duration(exper)).Unix()
	token := jwt.New(jwt.SigningMethodHS256)

	if addUser {
		rolesToJson, _ := json.Marshal(roles)
		claims := make(jwt.MapClaims)
		claims["exp"] = exp
		claims["iat"] = time.Now().Unix()
		claims["roles"] = string(rolesToJson)
		claims["id"], _ = uuid.Parse("2f8c572a-c81d-496f-9c62-710861f99fdf")
		token.Claims = claims
	}

	tokenString, err := token.SignedString([]byte(secretKey))

	if err != nil {
		return "", err
	}

	return tokenString, nil

}
